(ns pseudon.ruby_annotations)

(def ruby-annotations {
    :String {
        :split        [:string/split 0 1]
        :count        [:string/count 0 1]
    }
    :List {
        :push         [:list/push 0 1]
        :concat       [:list/extend 0 1]
    }
    :Any {
        :length       [:any/length 0]
        :to_s         [:any/to-string 0]
    }
    :Dict {
        :keys         [:dict/keys 0]
        :values       [:dict/values 0]
    }
})
