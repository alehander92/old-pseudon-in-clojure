(ns pseudon.perl_emitter
  (use [pseudon.code_formatter]
       [pseudon.node_types]
       [pseudon.lang_emitter])
  (:require pseudon.node_types)
  (:import [pseudon.node_types Cell PsClass Def PsName PsName4 PsInstanceName Method InstanceAssignment Assignment Args If Parent ImplicitReturn ExplicitReturn ConditionBody Elif Else PsInteger PsString]))
" emits perl code"

(defmulti emit-perl class)

(defmethod emit-perl :default [x] (str x))

(defn format-perl
  [code]
  (code-formatter code "    " {
    :after-end {"{" :indent}
    :before    {"}" :dedent}
  }))

(emitter emit-perl
  Cell                [[:z "\n" 0]]

  PsName              ["$" :name]

  PsInstanceName      ["$self->" :name]

  Assignment          [:target " = " :right]

  InstanceAssignment  [:target " = " :right]

  Def                 ["def " :name " {" :NL
                          "my (" :args ") = @_;" :NL
                           [:body "\n" 1] ";" :NL
                       "}" :NL]

  nil                 [""]

  ExplicitReturn      ["return " :object]

  ImplicitReturn      ["return " :object]

  If                  ["if " :jf :NL [:elif "\n"] :NL :else]

  Elif                ["else if " :object]

  Else                ["else" "{" [:object "\n" 1] :NL "}" :NL]

  ConditionBody       ["(" :condition "){" :NL
                        [:body "\n" 1] :NL
                        "}" :NL]

  Method              ["sub " :name " {" :NL
                           "my ($self, " :args ") = @_;" :NL
                           [:body "\n" 1] ";" :NL
                       "}" :NL]

  PsName4             [:name]

  Args                [[:args ", "]]

  PsClass             ["package " :name ";" :NL
                       :parent
                       "use Moo;" :NL
                       [:body "\n"] :NL]

  PsInteger           [:value]

  Parent              ["parent " :object ";" :NL])
