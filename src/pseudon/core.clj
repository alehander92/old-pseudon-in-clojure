(ns pseudon.core
  (use [pseudon.ast_formatter]
       [pseudon.python_emitter]
       [pseudon.ruby_emitter]
       [pseudon.perl_emitter]
       [clojure.string :only (split join)])

  (:gen-class :main true))

(defn save-as
  [source filename]
  (spit filename source))

(defn translate-file
  [filename target]
  (let [lang-emitters {:py emit-python :python emit-python :rb emit-ruby :ruby emit-ruby :pl emit-perl :perl emit-perl}
        lang-formatters {:rb format-ruby :ruby format-ruby :pl format-perl :perl format-perl}
        extensions {:py "py" :python "py" :rb "rb" :ruby "rb" :pl "pl" :perl "pl" :coffee "coffee" :coffeescript "coffee"}
        f (-> filename (str ".pseudon") slurp to-ast)]
      (println f)
      (-> f ((target lang-emitters)) ((target lang-formatters)) (save-as (str filename "." (target extensions))))))

(defn -main
  "Generates code for a pseudon file"
  [& args]
  (if (< (count args) 2)
    (println "Usage: java -jar pseudon.jar <filename> <lang>")
    (translate-file (first args) (keyword (second args)))))
