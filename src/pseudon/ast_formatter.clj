(ns pseudon.ast_formatter
  (use [defun :only (defun)])
  (use [pseudon.node_types])
  (:require pseudon.node_types)
  (:import [pseudon.node_types Cell PsClass Def PsName PsName4 PsInstanceName Method Assignment Args ImplicitReturn ExplicitReturn If Parent ConditionBody Elif Else]))

; formats clojure sexp ast as a pseudon tagged ast
(declare format-sexp)

(defn format-sexp-nodes
  [nodes]
  (map format-sexp nodes))

(defn extract-return
  [asts]
  (let [lastast (last asts)
        m (cond
      (isa? lastast ExplicitReturn) lastast
      (isa? lastast If) (let [ps-if (:jf lastast) ps-elif (map #(:object %) (:contexts lastast)) ps-else-obj (:el lastast) ps-else (if (nil? ps-else-obj) nil (:object ps-else-obj))] (->If
                            (->ConditionBody (:condition ps-if) (extract-return (:body ps-if)))
                            (map (fn [c] (->Elif (->ConditionBody (:condition c) (extract-return (:body c))))) ps-elif)
                            (->Else (if (nil? ps-else) nil (extract-return ps-else)))))
      :else (->ImplicitReturn lastast))]
      (conj (butlast asts) m)))

(defn format-sexp-def-nodes
  [nodes]
  (extract-return (map format-sexp nodes)))

(defun format-sexp
  ([(('cell & body) :seq)]
    (->Cell (format-sexp-nodes body)))
  ([(('class name 'parent parent & body) :seq)]
    (->PsClass (->PsName4 name) (->Parent (format-sexp parent)) (format-sexp-nodes body)))
  ([(('class name & body) :seq)]
    (->PsClass (->PsName4 name) nil (format-sexp-nodes body)))
  ([(('def name args & body) :seq)]
    (->Def (->PsName4 name) (->Args (format-sexp-nodes args)) (format-sexp-def-nodes body)))
  ([(('method name args & body) :seq)]
    (->Method (->PsName4 name) (->Args (format-sexp-nodes args)) (format-sexp-def-nodes body)))
  ([(('= (('clojure.core/deref target) :seq) right) :seq)]
    (->InstanceAssignment (->PsInstanceName target) (format-sexp right)))
  ([(('= target right) :seq)]
    (->Assignment (->PsName target) (format-sexp right)))
  ([(('clojure.core/deref name) :seq)]
    (->PsInstanceName name))
  ([(('if test body) :seq)]
    (->If (->ConditionBody test body) [] nil))
  ([(('if test body 'elif elif-test elif-body) :seq)]
    (->If (->ConditionBody test body) [(->Elif (->ConditionBody elif-test elif-body))] nil))
  ([(('if test body 'else else-body) :seq)]
    (->If (->ConditionBody test body) [] (->Else else-body)))
  ([(('do & body) :seq)]
    (format-sexp-nodes body))
  ([(('return expr) :seq)]
    (->ExplicitReturn (format-sexp expr)))
  ([name]
    (cond (integer? name) (->PsInteger name)
          :else           (->PsName name))))

(defn to-ast
  [source]
  (-> source read-string format-sexp))
