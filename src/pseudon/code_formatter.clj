(ns pseudon.code_formatter)

(defn- to-options
  [options]
  (merge {:before {} :before-end {} :after {} :after-end {}} options))

(defn- format-lines
  [firs next offset depth options]
  (let [tokens (clojure.string/split firs #" ")
        words (clojure.string/split next #" ")
        depth
          (cond (or
                (= :indent (get (:after options) (first tokens)))
                (= :indent (get (:after-end options) (last tokens)))
                (= :indent (get (:before options) (first words)))
                (= :indent (get (:before-end options) (last words))))
          (+ depth 1)
            (or (= :dedent (get (:after options) (first tokens)))
                (= :dedent (get (:after-end options) (last tokens)))
                (= :dedent (get (:before options) (first words)))
                (= :dedent (get (:before-end options) (last words))))
          (- depth 1)
            :else depth)]
      [depth (str (apply str (repeat depth offset))
           (clojure.string/join " " words))]))



(defn code-formatter
  [code offset options]
  (let [lines   (clojure.string/split code #"\n")
        options (to-options options)]
    (str (clojure.string/join
      "\n" (cons
        (first lines)
        (last (reduce
          (fn [[depth z] [line next]]
              (let [[depth2 a] (format-lines line next offset depth options)]
                  [depth2 (conj z a)]))
          [0 []]
          (map vector (butlast lines) (rest lines)))))) "\n")))
