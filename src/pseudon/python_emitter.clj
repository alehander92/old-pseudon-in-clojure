(ns pseudon.python_emitter
  (use [pseudon.node_types]
       [pseudon.lang_emitter])
  (:require pseudon.node_types)
  (:import [pseudon.node_types PsClass Def PsName PsInstanceName Method InstanceAssignment Assignment Args]))
" emits python code"

(defmulti emit-python
  (fn [x _ ] (println x)(class x)))

(defmethod emit-python :default [x o] (str x " " o))
; (defmethod emit-python PsName
;   [ast]
;   (:name ast))

; (defmethod emit-python PsInstanceName
;   [ast]
;   (str "self." (:name ast)))

(emitter emit-python)
  ; PsName              (:name)
  ;
  ; PsInstanceName      ("self." :name)
  ;
  ; Assignment          (:target " = " :right)
  ;
  ; InstanceAssignment  (:target " = " :right)
  ;
  ; Def                 ["def " :name "(" :args "):\n" [:body "\n" 1]]
  ;
  ; Method              ["def " :name "(self, " :args "):\n" [:body "\n" 1]]
  ;
  ; Args                [[:args ", "]]
  ;
  ; PsClass             ["class " :name ":\n" [:body "\n" 1]])
