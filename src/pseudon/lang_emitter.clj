(ns pseudon.lang_emitter
  (use [pseudon.node_types]
       [clojure.core.match :only (match)]
       [clojure.string :only (split join)]))

(defmacro z [& e] `(println ~@e))

(defn emit-safe
  [slot ast lang-emitter]
    (println "slot" slot)
    (let [s (-> ast slot)]
      (println "ast" ast s)
      (cond
        (symbol? s) s
        :else       (lang-emitter s))))

(defn emit-safe-nodes
  [part ast name]
  ; (println (nth part 0) ast (-> ast (nth part 0)))
  (let [z (nth part 0)
        s (z ast)
        join_arg (nth part 1)]
      (println "safe" z s (:object s))
      (apply str (interpose join_arg (map (fn [z] (name z)) s)))))

(defn emit-builtin
  [part]
  (z "EMITBUILTIN" part)
  (let [l 2]
    (cond
      (= part :NL) "\n"
      (= part :TAB) "\t"
      (= part :WS) " "
      (= part :DOT) "."
      (= part :COMMA) ","
      (= part :INDENT) "    "
      :else        "")))

(defmacro t-part
  [part name ast]
  (println "x" part)
  (cond
    (keyword? part)
      (if (Character/isLowerCase (-> part str second)) ;:NL second is N
        `(emit-safe ~part ~ast ~name)
        `(emit-builtin ~part))
    (vector? part)  `(emit-safe-nodes ~part ~ast ~name)
    :else       part
))

(defmacro t
  [parts ast name]
  (println "t" ast)
  (let [u0 `(t-part ~(first parts) ~name ~ast)
        u   (map (fn [part] `(t-part ~part ~name ~ast)) (rest parts))]
    `(apply str (list ~u0 ~@u))))


(defmacro emitter
  [name & exprs]
  (let
    [methods (for [[type body] (partition 2 exprs)]
      `(defmethod ~name ~type [ast#] (t ~body ast# ~name)))]
    `(do ~@methods)))
